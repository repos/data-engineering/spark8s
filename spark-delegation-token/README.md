# spark-delegation-token

## Definition

Cli to retrieve Hadoop Delegation Token for HDFS and hive and store them in local
temporary file

Those token have a 24 hours lifetime.
It is possible to renew them (up to 7 times) but only from a kerberos authenticated session.

This Cli is called by the spark8s before submitting a SparkApplication to the K8S DSE cluster

## Usage

Executing the command below on an analytics client (aka stat server) will generate a token credential file containing
an hdfs and hive delegation token.

```shell
java -cp spark-delegation-token-1.0-SNAPSHOT.jar:/etc/hive/conf:$(hadoop classpath):/usr/lib/hive/lib/* org.wikimedia.kubernetes.hadoop.security.Main ./token
```

In order to ensure usage of the hive hadoop delegation token below configuration must be added to the hive-site.xml
configuration for the job relying on it

```xml
<property>
  <name>hive.metastore.token.signature</name>
  <value>DelegationTokenForHiveMetaStoreServer</value>
</property>
```

## Build

```shell
make build
```

## Test

```shell
make check
make test
```
