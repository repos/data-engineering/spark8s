# spark8s

Cli used to submit spark jobs on the DSE K8S cluster

## Usage

## Setup

make command must be available to run build and tests locally
jkd 1.8 and maven must be available
```shell
export JAVA_HOME=<path_jdk8>
```

### Install pre-commit hook
- install pre-commit python package

```shell
pip install pre-commit
```

- install hook locally

```shell
make .git/hook/pre-commit
```

## Build

```shell
make build
```

## Test

```shell
make check
make test
```
