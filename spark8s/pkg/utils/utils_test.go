/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	apiv1 "k8s.io/api/core/v1"
	"os"
	"testing"
)

func TestGetNewSparkApplicationName(t *testing.T) {
	uuid := "uuid-1"
	sparkAppName := GetNewSparkApplicationName(uuid)
	assert.Equal(t, sparkAppName, fmt.Sprintf("%s-%s-%s", RootSparkAppName, os.Getenv("USER"), uuid))
}

func TestGetBase64EncodedFileData(t *testing.T) {
	file, err := os.CreateTemp(os.TempDir(), "base64")
	if err != nil {
		t.Fatalf("Failed to create temporary file  %v", err)
	}
	defer os.Remove(file.Name())

	file.WriteString("test base64 encoding")

	base6AEncodedData, err := GetBase64EncodedFileData(file.Name())
	if err != nil {
		t.Fatalf("Failed to base64 encode file data %s  %v", file.Name(), err)
	}

	assert.Equal(t, base6AEncodedData, "dGVzdCBiYXNlNjQgZW5jb2Rpbmc=")
}

func TestGetBase64EncodedFileDataFailedOnNonExistingFile(t *testing.T) {
	_, err := GetBase64EncodedFileData("non_existing_file")
	if err == nil {
		t.Fatal("Must failed as the file non_existing_file doesn't exist")
	}
}

func TestContains(t *testing.T) {
	item := "item1"
	items := []string{"item1", "item2"}
	assert.True(t, Contains(item, items))

	item = "not_in_the_list"
	assert.False(t, Contains(item, items))
}

func TestCreateObjectFromTemplate(t *testing.T) {
	sparkApp := SparkApp{
		Name:      "spark",
		NameSpace: NameSpace,
		Image:     Image,
		Version:   Version,
		Secret: Secret{
			Name: "secret",
			Data: "ZGF0YQo=",
		},
	}
	secret := &apiv1.Secret{}
	CreateObjectFromTemplate(&sparkApp, SecretTemplate, secret)
	assert.Equal(t, secret.Name, "secret")
	assert.Equal(t, secret.Type, apiv1.SecretType("HadoopDelegationToken"))
	token, ok := secret.Data["hadoop.token"]
	if !ok {
		t.Fatal("Failed to get hadoop.token from secret")
	}
	assert.Equal(t, string(token), "data\n")
}

func TestRunCommandSucceed(t *testing.T) {
	rootTempPath, err := os.MkdirTemp(os.TempDir(), "spark_test")
	if err != nil {
		t.Fatalf("Failed to create root temporary folder %v", err)
	}

	// Command run succeed
	if _, err = RunCommand("ls", rootTempPath); err != nil {
		t.Fatalf("Failed to execute ls command %v", err)
	}
}

func TestRunCommandFailed(t *testing.T) {
	if _, err := RunCommand("not_existing_command"); err == nil {
		t.Fatalf("Command run should failed %v", err)
	}
}
