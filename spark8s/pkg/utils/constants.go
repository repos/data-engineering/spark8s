/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

const (
	RootSparkAppName = "spark-app"
)

// SparkApplicationTemplate
const (
	NameSpace = "spark"
	Image     = "docker-registry.wikimedia.org/spark:3.3.0-2"
	Version   = "3.3.0"
	// #nosec G101
	SecretTemplate = `apiVersion: v1
kind: Secret
metadata:
  name: {{ .Secret.Name }}
  namespace: spark
type: HadoopDelegationToken
data:
  hadoop.token: {{ .Secret.Data }}`
	SparkApplicationTemplate = `apiVersion: "sparkoperator.k8s.io/v1beta2"
kind: SparkApplication
metadata:
  name: {{ .Name }}
  namespace: {{ .NameSpace }}
spec:
  type: {{ .Spec.Type }}
  mode: {{ .Spec.Mode }}
  image: "{{ .Image }}"
  imagePullPolicy: IfNotPresent
  mainClass: {{ .Spec.MainClass }}
  mainApplicationFile: "{{ .Spec.MainApplicationFile }}"
  arguments: {{ .Spec.Arguments }}
  sparkVersion: "{{ .Version }}"
  restartPolicy:
    type: Never
  sparkConf:
    spark.driver.port: "12000"
    spark.driver.blockManager.port: "13000"
    spark.blockManager.port: "13000"
    spark.ui.port: "4045"
  driver:
    cores: {{ .Spec.Driver.Cores }}
    coreLimit: "{{ .Spec.Driver.CoresLimit }}"
    memory: "{{ .Spec.Driver.Memory }}"
    labels:
      version: "{{ .Version }}"
    serviceAccount: spark-driver
    secrets:
      - name: {{ .Secret.Name }}
        path: /mnt/secrets
        secretType: HadoopDelegationToken
  executor:
    cores: {{ .Spec.Executor.Cores }}
    coreLimit: "{{ .Spec.Executor.CoresLimit }}"
    memory: "{{ .Spec.Executor.Memory }}"
    instances:  {{ .Spec.ExecutorInstances }}
    labels:
      version: "{{ .Version }}"
    secrets:
      - name: {{ .Secret.Name }}
        path: /mnt/secrets
        secretType: HadoopDelegationToken`
)

// HadoopDelegationToken
const (
	HadoopDelegationTokenClassPath = "/usr/lib/spark8s/lib/spark-delegation-token.jar:/etc/hive/conf:/usr/lib/hive/lib/*" // #nosec G101
	HadoopDelegationTokenMainCLass = "org.wikimedia.kubernetes.hadoop.security.Main"                                      // #nosec G101
)
