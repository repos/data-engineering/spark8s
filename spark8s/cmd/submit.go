/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"errors"
	"fmt"
	"github.com/GoogleCloudPlatform/spark-on-k8s-operator/pkg/apis/sparkoperator.k8s.io/v1beta2"
	sparkclientset "github.com/GoogleCloudPlatform/spark-on-k8s-operator/pkg/client/clientset/versioned"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"org/wikimedia/spark8s/pkg/utils"
	"os"
	"path/filepath"
)

func getRootTempPath() string {
	return fmt.Sprintf("%s/.spark8s", os.Getenv("HOME"))
}

var SparkAppName = utils.GetNewSparkApplicationName(utils.GetUuid())

var DefaultRootTempPath = getRootTempPath()
var RootTempPath string

var Type string
var Types = []string{"Java", "Scala", "Python", "R"}
var Mode string
var Modes = []string{"cluster", "client", "in-cluster-client"}

var MainClass string
var MainApplicationFile string
var Arguments []string

var DriverCores float32
var DriverCoresLimit string
var DriverMemory string

var ExecutorCores float32
var ExecutorCoresLimit string
var ExecutorMemory string
var ExecutorInstances int32

// submitCmd represents the submit command
var submitCmd = &cobra.Command{
	Use:   "submit",
	Short: "Submit a spark application within K8S cluster",
	Long:  `Submit a spark application with all required credentials in the K8S cluster.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := submitSparkApplication(); err != nil {
			log.Fatal("Failed to submit spark application on K8S cluster. ", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(submitCmd)

	// Set flags
	rootCmd.PersistentFlags().StringVar(&RootTempPath, "root-temp-dir", DefaultRootTempPath,
		"Root folder where temporary folder will be created with credentials, sparkapplication yaml, ...")

	rootCmd.PersistentFlags().StringVar(&Type, "type", "Scala", "Type of spark application (Java, Scala, Python, R)")
	rootCmd.PersistentFlags().StringVar(&Mode, "mode", "cluster", "Deployment mode (cluster, client, in-cluster-client)")

	rootCmd.PersistentFlags().StringVar(&MainApplicationFile, "main-application-file", "", "Main application file (jar, py,...)")
	rootCmd.PersistentFlags().StringVar(&MainClass, "main-class", "", "Main class for the spark application")
	rootCmd.PersistentFlags().StringSliceVar(&Arguments, "arguments", []string{}, "Arguments of the spark application")

	rootCmd.PersistentFlags().Float32Var(&DriverCores, "driver-cores", 1, "Requested cores for driver")
	rootCmd.PersistentFlags().StringVar(&DriverCoresLimit, "driver-cores-limits", "1", "Limit cores for driver")
	rootCmd.PersistentFlags().StringVar(&DriverMemory, "driver-memory", "512m", "Memory for driver")

	rootCmd.PersistentFlags().Float32Var(&ExecutorCores, "executor-cores", 1, "Requested cores for executor")
	rootCmd.PersistentFlags().StringVar(&ExecutorCoresLimit, "executor-cores-limits", "1", "Limit cores for executor")
	rootCmd.PersistentFlags().StringVar(&ExecutorMemory, "executor-memory", "512m", "Memory for executor")
	rootCmd.PersistentFlags().Int32Var(&ExecutorInstances, "executor-instances", 1, "Number of executor instances")

	// Set required flags
	if err := rootCmd.MarkPersistentFlagRequired("main-application-file"); err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
}

func submitSparkApplication() error {
	// Validate Flag
	if err := validateFlag(); err != nil {
		return err
	}
	log.WithFields(log.Fields{"name": SparkAppName}).Info("Creating SparkApplication")

	// Build kube config
	config, err := clientcmd.BuildConfigFromFlags("", KubeConfig)
	if err != nil {
		return fmt.Errorf("failed to build kube config with config file %s: %v", KubeConfig, err)
	}
	// Create the kube client
	kubeClientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return fmt.Errorf("failed to create kube client set: %v", err)
	}
	// Create the spark kube client
	sparkClientSet, err := sparkclientset.NewForConfig(config)
	if err != nil {
		return fmt.Errorf("failed to create spark kube client set: %v", err)
	}

	// Create temporary folder to be deleted at the end of the program
	tempPath, err := createTemporaryFolder(RootTempPath)
	if err != nil {
		return err
	}
	// Ensure the temporary folder is removed with all it's content at the end
	defer os.RemoveAll(tempPath)

	// Create Hadoop Delegation Token
	hdtCredentialPath := filepath.Join(tempPath, "hdt-credentials")
	if err = createHadoopDelegationTokenCredentialFile(hdtCredentialPath); err != nil {
		return err
	}

	// Create secret and spark application inside the kubernetes cluster
	sparkApp, err := getSparkApplicationObject(hdtCredentialPath)
	if err != nil {
		return err
	}
	if err = createSecret(kubeClientSet, sparkApp); err != nil {
		return err
	}
	if err = createSparkApplication(sparkClientSet, sparkApp); err != nil {
		return err
	}

	return nil
}

func validateFlag() error {
	if !utils.Contains(Type, Types) {
		return fmt.Errorf("define type is not authorized: %s - %v", Type, Types)
	}
	if !utils.Contains(Mode, Modes) {
		return fmt.Errorf("define more is not authorized: %s - %v", Mode, Modes)
	}
	if !utils.Contains(Type, []string{"Java", "Scala"}) {
		// Main class must be defined for Java and Scala
		if MainClass == "" {
			return errors.New("main-class must be define")
		}
	}
	return nil
}

func createTemporaryFolder(rootTempPath string) (string, error) {
	// Create root temp folder
	stat, err := os.Stat(rootTempPath)
	if os.IsNotExist(err) {
		log.WithFields(log.Fields{"root_temp": rootTempPath}).Info("Create root temporary folders")
		if err := os.MkdirAll(rootTempPath, os.ModePerm); err != nil {
			return "", fmt.Errorf("failed to create folders %s. %v", rootTempPath, err)
		}
	} else if err != nil {
		return "", fmt.Errorf("failed to get stats on %s. %v", rootTempPath, err)
	} else if !stat.IsDir() {
		return "", fmt.Errorf("%s exist but is not a directory", rootTempPath)
	}

	// Create temp folder for current spark application
	tempPath := filepath.Join(rootTempPath, SparkAppName)
	log.WithFields(log.Fields{"folder": tempPath}).Info("Create application temporary folders")
	if err := os.Mkdir(tempPath, 0700); err != nil {
		return "", fmt.Errorf("failed to create application temporary folder %s. %v", tempPath, err)
	}

	return tempPath, nil
}

func createHadoopDelegationTokenCredentialFile(hdtCredentialPath string) error {
	log.Debug("Get hadoop classpath")
	hadoopClassPath, err := utils.RunCommand("hadoop", "classpath")
	if err != nil {
		return fmt.Errorf("failed to get hadoop classpath: %v", err)
	}

	log.WithFields(log.Fields{"creds": hdtCredentialPath}).Info("Create hadoop delegation token credential file")
	if _, err := utils.RunCommand("java", "-Djava.library.path=$HADOOP_HOME/lib/native", "-cp", fmt.Sprintf("%s:%s", utils.HadoopDelegationTokenClassPath, hadoopClassPath),
		utils.HadoopDelegationTokenMainCLass, hdtCredentialPath); err != nil {
		return fmt.Errorf("failed to execute hadoop delegation token command line: %v", err)
	}

	return nil
}

func getSparkApplicationObject(hdtCredentialPath string) (*utils.SparkApp, error) {
	log.Info("Get internal SparkApp object for template rendering")

	credsBase64Enc, err := utils.GetBase64EncodedFileData(hdtCredentialPath)
	if err != nil {
		return &utils.SparkApp{}, fmt.Errorf("failed to base64 encode hadoop delegation token: %v", err)
	}

	secret := utils.Secret{
		Name: SparkAppName,
		Data: credsBase64Enc,
	}
	driver := utils.PodSpec{
		Cores:      DriverCores,
		CoresLimit: DriverCoresLimit,
		Memory:     DriverMemory,
	}
	executor := utils.PodSpec{
		Cores:      ExecutorCores,
		CoresLimit: ExecutorCoresLimit,
		Memory:     ExecutorMemory,
	}
	sparkAppSpec := utils.SparkAppSpec{
		Type:                Type,
		Mode:                Mode,
		MainClass:           MainClass,
		MainApplicationFile: MainApplicationFile,
		Arguments:           Arguments,
		Driver:              driver,
		Executor:            executor,
		ExecutorInstances:   ExecutorInstances,
	}
	sparkApp := utils.SparkApp{
		Name:      SparkAppName,
		NameSpace: utils.NameSpace,
		Image:     utils.Image,
		Version:   utils.Version,
		Secret:    secret,
		Spec:      sparkAppSpec,
	}

	return &sparkApp, nil
}

func createSecret(kubeClientSet *kubernetes.Clientset, sparkApp *utils.SparkApp) error {
	secret := &apiv1.Secret{}
	if err := utils.CreateObjectFromTemplate(sparkApp, utils.SecretTemplate, secret); err != nil {
		return fmt.Errorf("failed to create Secret object : %v", err)
	}

	log.WithFields(log.Fields{"name": SparkAppName}).Info("Create Secret into kubernetes")
	if _, err := kubeClientSet.CoreV1().Secrets(utils.NameSpace).Create(context.TODO(), secret, metav1.CreateOptions{}); err != nil {
		return fmt.Errorf("failed to create secret in kubernetes: %v", err)
	}

	return nil
}

func createSparkApplication(sparkClientSet *sparkclientset.Clientset, sparkApp *utils.SparkApp) error {
	sparkApplication := &v1beta2.SparkApplication{}
	if err := utils.CreateObjectFromTemplate(sparkApp, utils.SparkApplicationTemplate, sparkApplication); err != nil {
		return fmt.Errorf("failed to create SparkApplication object : %v", err)
	}

	log.WithFields(log.Fields{"name": SparkAppName}).Info("Create SparkApplication into kubernetes")
	if _, err := sparkClientSet.SparkoperatorV1beta2().SparkApplications(utils.NameSpace).Create(context.TODO(), sparkApplication, metav1.CreateOptions{}); err != nil {
		return fmt.Errorf("failed to create spark application in kubernetes: %v", err)
	}

	return nil
}
