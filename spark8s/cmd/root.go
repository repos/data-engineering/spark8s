/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func getKubeConfigPath() string {
	var kubeConfigEnv = os.Getenv("KUBECONFIG")
	if len(kubeConfigEnv) == 0 {
		return "/etc/kubernetes/spark-deploy-dse-k8s-eqiad.config"
	}
	return kubeConfigEnv
}

var defaultKubeConfig = getKubeConfigPath()

var KubeConfig string
var DebugLog bool

var rootCmd = &cobra.Command{
	Use:   "spark8s",
	Short: "spark8s is the command-line tool for managing spark application within the DSE K8S",
	Long:  `spark8s is the command-line tool for managing spark application within the DSE K8S.`,
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&KubeConfig, "kubeconfig", "k", defaultKubeConfig,
		"The path to the local Kubernetes configuration file")
	rootCmd.PersistentFlags().BoolVar(&DebugLog, "debug", false, "Print debug logs")
}

func Execute() {
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})
	log.SetLevel(log.InfoLevel)
	if DebugLog {
		log.SetLevel(log.DebugLevel)
	}

	err := rootCmd.Execute()
	if err != nil {
		log.Fatal("Failed to execute spark8s cli.", err)
		os.Exit(1)
	}
}
