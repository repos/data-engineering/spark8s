# spark8s

## Definition

Cli to interact with spark application inside the kubernetes cluster

## Usage

```shell
stat1005:~$spark8s
spark8s is the command-line tool for managing spark application within the DSE K8S.

Usage:
spark8s [command]

Available Commands:
completion  Generate the autocompletion script for the specified shell
help        Help about any command
submit      Submit a spark application within K8S cluster

Flags:
--arguments strings              Arguments of the spark application
--debug                          Print debug logs
--driver-cores float32           Requested cores for driver (default 1)
--driver-cores-limits string     Limit cores for driver (default "1")
--driver-memory string           Memory for driver (default "512m")
--executor-cores float32         Requested cores for driver (default 1)
--executor-cores-limits string   Limit cores for driver (default "1")
--executor-instances int32       Number of executor instances (default 1)
--executor-memory string         Memory for driver (default "512m")
-h, --help                           help for spark8s
-k, --kubeconfig string              The path to the local Kubernetes configuration file (default "/etc/kubernetes/spark-deploy-dse-k8s-eqiad.config")
--main-application-file string   Main application file (jar, py,...)
--main-class string              Main class for the spark application
--mode string                    Deployment mode (cluster, client, in-cluster-client) (default "cluster")
--root-temp-dir string           Root folder where temporary folder will be created with credentials, sparkapplication yaml, ... (default "$HOME/.spark8s")
--type string                    Type of spark application (Java, Scala, Python, R) (default "Scala")

Use "spark8s [command] --help" for more information about a command.
```

### Submit

Submit a spark application inside the kubernetes cluster:
- creation of the hadoop delegation token
- creation of the spark application yaml definition and related kubernetes objects (secret,...) thanks to templating
- submit all kubernetes objects and spark application to the kubernetes cluster

```shell
stat1005:~$ ./spark8s submit --help
Submit a spark application with all required credentials in the K8S cluster.

Usage:
spark8s submit [flags]

Flags:
-h, --help   help for submit

Global Flags:
--arguments strings              Arguments of the spark application
--debug                          Print debug logs
--driver-cores float32           Requested cores for driver (default 1)
--driver-cores-limits string     Limit cores for driver (default "1")
--driver-memory string           Memory for driver (default "512m")
--executor-cores float32         Requested cores for driver (default 1)
--executor-cores-limits string   Limit cores for driver (default "1")
--executor-instances int32       Number of executor instances (default 1)
--executor-memory string         Memory for driver (default "512m")
-k, --kubeconfig string              The path to the local Kubernetes configuration file (default "/etc/kubernetes/spark-deploy-dse-k8s-eqiad.config")
--main-application-file string   Main application file (jar, py,...)
--main-class string              Main class for the spark application
--mode string                    Deployment mode (cluster, client, in-cluster-client) (default "cluster")
--root-temp-dir string           Root folder where temporary folder will be created with credentials, sparkapplication yaml, ... (default "$HOME/.spark8s")
--type string                    Type of spark application (Java, Scala, Python, R) (default "Scala")
```

Ex.:
```shell
spark8s submit --kubeconfig config --main-class org.apache.spark.examples.HdfsTest --main-application-file local:///opt/spark/examples/jars/spark-examples_2.12-3.3.0.jar```
```

## Build

```shell
make build
```

## Test

Check rule require a set of specific dependencies. To install those dependencies run:
```shell
make check-dependency
```

Run tests:
```shell
make check # lint, sec
make test # unittest
```

## Add new sub command

This cli rely on [cobra](https://github.com/spf13/cobra/blob/main/user_guide.md) library.

This library provide a cobra cli to generate skeleton for sub-command.
Run below command to install this cli

```shell
make dependency
```

_Optional: you can create a ~/.cobra.yaml config file to ensure appropriate licensing is added to_
the file

```yaml
author: Wikimedia Foundation
license: apache
useViper: flase
```

Then execute below command to add a sub-command

```shell
cobra-cli add <sub-command-name>
```

It will create a <sub-command-name>.go file in cmd which is the file where the flags and logic
to be run should be call
